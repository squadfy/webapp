import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'equipes-alocar',
  templateUrl: './equipes-alocar.component.html',
  styleUrls: ['./equipes-alocar.component.scss']
})
export class EquipesAlocarComponent implements OnInit {

  @Input('equipes') equipes

  constructor() { }

  ngOnInit() {
  }

}
