import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipesAlocarComponent } from './equipes-alocar.component';

describe('EquipesAlocarComponent', () => {
  let component: EquipesAlocarComponent;
  let fixture: ComponentFixture<EquipesAlocarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipesAlocarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipesAlocarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
