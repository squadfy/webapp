import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'detalhes-tarefa',
  templateUrl: './detalhes-tarefa.component.html',
  styleUrls: ['./detalhes-tarefa.component.scss']
})
export class DetalhesTarefaComponent implements OnInit {

  @Input('detalhesTarefa') detalhesTarefa

  constructor() { }

  ngOnInit() {
  }

}
