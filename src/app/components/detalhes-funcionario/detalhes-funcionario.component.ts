import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'detalhes-funcionario',
  templateUrl: './detalhes-funcionario.component.html',
  styleUrls: ['./detalhes-funcionario.component.scss']
})
export class DetalhesFuncionarioComponent implements OnInit {

  @Input('detalhesFuncionario') detalhesFuncionario

  constructor() { }

  ngOnInit() {
  }

}
