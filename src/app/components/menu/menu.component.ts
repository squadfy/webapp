import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  Navigate(route, li) {
    document.getElementById('1').classList.remove('active')
    document.getElementById('2').classList.remove('active')

    document.getElementById(li).classList.add('active')
 
    this.router.navigateByUrl(route);
  }
}
