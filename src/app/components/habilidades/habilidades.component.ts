import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'habilidades',
  templateUrl: './habilidades.component.html',
  styleUrls: ['./habilidades.component.scss']
})
export class HabilidadesComponent implements OnInit {

  @Input('habilidades') habilidades

  constructor() { }

  ngOnInit() {
  }
}
