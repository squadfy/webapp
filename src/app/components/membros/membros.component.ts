import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'membros',
  templateUrl: './membros.component.html',
  styleUrls: ['./membros.component.scss']
})
export class MembrosComponent implements OnInit {

  @Input('membros') membros

  constructor() { }

  ngOnInit() {
  }

}
