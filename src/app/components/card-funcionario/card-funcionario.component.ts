import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'card-funcionario',
  templateUrl: './card-funcionario.component.html',
  styleUrls: ['./card-funcionario.component.scss']
})
export class CardFuncionarioComponent implements OnInit {

  @Input('funcionario') funcionario
  @Output('AbrirDetalhesFuncionario') AbrirDetalhesFuncionario = new EventEmitter()
  private detalhes

  listaHabilidades: any[]

  constructor() { }

  ngOnInit() {
    this.listaHabilidades = this.funcionario.habilidades ? this.funcionario.habilidades : ['Adicionar no POST da API']
  }

  AbrirDetalhes() {
    this.AbrirDetalhesFuncionario.emit(this.funcionario)
  }
}
