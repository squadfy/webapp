import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'sub-menu',
  templateUrl: './sub-menu.component.html',
  styleUrls: ['./sub-menu.component.scss']
})
export class SubMenuComponent implements OnInit {

  @Input('titulo') titulo

  constructor
  (
    private router: Router
  ) { }

  ngOnInit() {
  }

  AdicionarProjeto() {
    this.router.navigate(['tabs', 'dashboard', 'add-projeto'])
  }

}
