import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TabsComponent } from './pages/tabs/tabs.component';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { MenuComponent } from './components/menu/menu.component';
import { ListaFuncionariosComponent } from './pages/lista-funcionarios/lista-funcionarios.component';
import { CardProjetoComponent } from './components/card-projeto/card-projeto.component';
import { SubMenuComponent } from './components/sub-menu/sub-menu.component';
import { CardTarefaComponent } from './components/card-tarefa/card-tarefa.component';
import { MembrosComponent } from './components/membros/membros.component';
import { HabilidadesComponent } from './components/habilidades/habilidades.component';
import { DetalhesTarefaComponent } from './components/detalhes-tarefa/detalhes-tarefa.component';
import { AdicionarProjetoComponent } from './pages/adicionar-projeto/adicionar-projeto.component';
import { AdicionarTarefaComponent } from './pages/adicionar-tarefa/adicionar-tarefa.component';
import { AdicionarFuncionarioComponent } from './pages/adicionar-funcionario/adicionar-funcionario.component';
import { CardFuncionarioComponent } from './components/card-funcionario/card-funcionario.component';
import { DetalhesFuncionarioComponent } from './components/detalhes-funcionario/detalhes-funcionario.component';
import { EquipesAlocarComponent } from './components/equipes-alocar/equipes-alocar.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuthService } from './services/auth/auth.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    TabsComponent,
    LoginComponent,
    DashboardComponent,
    MenuComponent,
    ListaFuncionariosComponent,
    CardProjetoComponent,
    SubMenuComponent,
    CardTarefaComponent,
    MembrosComponent,
    HabilidadesComponent,
    DetalhesTarefaComponent,
    AdicionarProjetoComponent,
    AdicionarTarefaComponent,
    AdicionarFuncionarioComponent,
    CardFuncionarioComponent,
    DetalhesFuncionarioComponent,
    EquipesAlocarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
