import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adicionar-tarefa',
  templateUrl: './adicionar-tarefa.component.html',
  styleUrls: ['./adicionar-tarefa.component.scss']
})
export class AdicionarTarefaComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  AdicionarTarefa() {
    this.router.navigate(['tabs', 'dashboard'])
  }

}
