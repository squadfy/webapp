import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private loginGroup: FormGroup

  constructor
  (
    private router: Router,
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) 
  { 
    this.loginGroup = formBuilder.group({
      user: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  ngOnInit() {
  }

  Logar() {
    document.getElementById('area-login').classList.add('logar')

    setTimeout(() => {
      this.authService.Logar(this.loginGroup.value).subscribe(res => {
        this.router.navigate(['tabs', 'dashboard'])
      }, err => {
        console.log(err)
        document.getElementById('area-login').classList.remove('logar')
      })
    }, 500)
  }
}
