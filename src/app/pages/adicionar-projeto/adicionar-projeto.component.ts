import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adicionar-projeto',
  templateUrl: './adicionar-projeto.component.html',
  styleUrls: ['./adicionar-projeto.component.scss']
})
export class AdicionarProjetoComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  AdicionarProjeto() {
    this.router.navigate(['tabs', 'dashboard'])
  }
}
