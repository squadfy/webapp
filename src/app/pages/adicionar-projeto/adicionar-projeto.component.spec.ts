import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarProjetoComponent } from './adicionar-projeto.component';

describe('AdicionarProjetoComponent', () => {
  let component: AdicionarProjetoComponent;
  let fixture: ComponentFixture<AdicionarProjetoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarProjetoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarProjetoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
