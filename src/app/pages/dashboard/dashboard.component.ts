import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  detalhesTarefa = {
    Nome: "Implementar tela de login do SquadFy",
    DataCriacao: "14/09/2019",
    Descricao: "Praesent consectetur aliquam hendrerit. Aenean vitae dapibus nibh. Ut vulputate nulla eu dolor finibus, sed tristique purus mattis. Aliquam id felis in sem eleifend venenatis a eu mauris. Suspendisse at mauris viverra nunc congue auctor nec.",
    Habilidades: ['HTML', 'CSS', 'JavaScript'],
    Membros: [1],
    Custo: "2000,00"
  }

  constructor
  (
    private router: Router
  ) { }

  ngOnInit() {
  }

  AdicionarTarefa() {
    this.router.navigate(['tabs', 'dashboard', 'add-tarefa'])
  }
}
