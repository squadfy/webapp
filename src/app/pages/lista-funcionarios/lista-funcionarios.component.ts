import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-funcionarios',
  templateUrl: './lista-funcionarios.component.html',
  styleUrls: ['./lista-funcionarios.component.scss']
})
export class ListaFuncionariosComponent implements OnInit {

  listaFuncionario = []
  detalhes

  constructor
  (
    private router: Router,
    private httpClient: HttpClient
  ) 
  { 
    this.InicializarListaUsuarios()
  }

  ngOnInit() {
  }

  AdicionarFuncionario() {
    this.router.navigate(['tabs', 'lista-funcionarios', 'add-funcionario'])
  }

  InicializarListaUsuarios() {
    this.httpClient.get(`${environment.url}/Usuario`).subscribe((res: any) => {
      this.listaFuncionario = res
    }, err => alert('Algo deu errado!'))
  }
  
  AbrirDetalhes(funcionario) {
    this.detalhes = funcionario
    console.log(this.detalhes)
  }
}
