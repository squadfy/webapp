import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-adicionar-funcionario',
  templateUrl: './adicionar-funcionario.component.html',
  styleUrls: ['./adicionar-funcionario.component.scss']
})
export class AdicionarFuncionarioComponent implements OnInit {

  funcionarioGroup: FormGroup

  constructor
  (
    private httpClient: HttpClient,
    private formBuilder: FormBuilder
  ) 
  { 
    this.funcionarioGroup = formBuilder.group({
      nome: [''],
      login: [''],
      senha: [''],
      salario: ['']
    })
  }

  ngOnInit() {
  }

  AdicionarFuncionario() {
    this.httpClient.post(`${environment.url}/Usuario`, {
      nome: this.funcionarioGroup.controls.nome.value,
      login: this.funcionarioGroup.controls.login.value,
      senha: this.funcionarioGroup.controls.senha.value,
      salario: this.funcionarioGroup.controls.salario.value,
      idTipoUsuario: 2
    }).subscribe(res => {
      console.log(res)
    }, err => console.log(err))
  }
}
