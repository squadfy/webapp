import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _url = environment.url

  constructor
  (
    private httpClient: HttpClient
  ) 
  { }

  Logar(credentials) {
    return this.httpClient.get(`${this._url}/Usuario/Login/${credentials.user}/${credentials.password}`)
  }
}
