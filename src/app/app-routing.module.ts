import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TabsComponent } from './pages/tabs/tabs.component';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ListaFuncionariosComponent } from './pages/lista-funcionarios/lista-funcionarios.component';
import { AdicionarProjetoComponent } from './pages/adicionar-projeto/adicionar-projeto.component';
import { AdicionarTarefaComponent } from './pages/adicionar-tarefa/adicionar-tarefa.component';
import { AdicionarFuncionarioComponent } from './pages/adicionar-funcionario/adicionar-funcionario.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'tabs', component: TabsComponent, 
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent, 
        children: [
          { path: 'add-projeto', component: AdicionarProjetoComponent },
          { path: 'add-tarefa', component: AdicionarTarefaComponent },
        ]
      },
      { path: 'lista-funcionarios', component: ListaFuncionariosComponent, 
        children: [
          { path: 'add-funcionario', component: AdicionarFuncionarioComponent },
        ]
      },
    ] 
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
